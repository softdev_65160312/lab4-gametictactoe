package com.aphidech.gametictactoe;

public class Tables {

    char[][] tables;
    Player player1;
    Player player2;
    Player currentPlayer;

    Tables() {
        char[][] tab = {{'_', '_', '_'}, {'_', '_', '_'}, {'_', '_', '_'}};
        player1 = new Player('X');
        player2 = new Player('O');
        this.currentPlayer = player1;
        this.tables = tab;
    }

    public boolean isCellMarked(int row, int col) {
        return tables[row - 1][col - 1] != '_';
    }

    public void setTable(int row, int col) {
        tables[row - 1][col - 1] = currentPlayer.getSymbol();
    }

    public char[][] getTable() {
        return tables;
    }

    public void switchPlayer() {
        currentPlayer = (currentPlayer.getSymbol() == player1.getSymbol()) ? player2 : player1;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public boolean checkWin() {
        var p = this.currentPlayer.getSymbol();
        for (int i = 0; i < 3; i++) {
            if (tables[i][0] == p && tables[i][1] == p && tables[i][2] == p) {
                return true;
            }
        }
        for (int i = 0; i < 3; i++) {
            if (tables[0][i] == p && tables[1][i] == p && tables[2][i] == p) {
                return true;
            }
        }
        if (tables[0][0] == p && tables[1][1] == p && tables[2][2] == p) {
            return true;
        }
        if (tables[0][2] == p && tables[1][1] == p && tables[2][0] == p) {
            return true;
        }
        return false;
    }

    public boolean checkDraw() {
        if (Game.getTurnCount() == 8) {
            return true;
        }
        return false;
    }
}
