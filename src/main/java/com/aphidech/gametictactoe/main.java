package com.aphidech.gametictactoe;

public class main {

    public static void main(String[] args) {
        Game xo = new Game();
        do {
            xo.play();
            xo.resetGame();
        } while (xo.checkContinue());
    }
}
