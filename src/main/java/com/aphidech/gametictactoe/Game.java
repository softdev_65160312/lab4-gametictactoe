package com.aphidech.gametictactoe;

import java.util.Scanner;

public final class Game {

    Player player;
    Tables tables;
    static int turnCount = 0;

    Game() {
        tables = new Tables();
        player = tables.getCurrentPlayer();
    }

    public void play() {
        displayWelcome();
        while (true) {
            displayTable();
            player = tables.getCurrentPlayer();
            playerInput();
            if (tables.checkWin()) {
                displayTable();
                System.out.println("player " + player.getSymbol() + " WIN!");
                break;
            }
            if (tables.checkDraw()) {
                displayTable();
                System.out.println("DRAW!");
                break;
            }
            tables.switchPlayer();
        }
    }

    public void displayWelcome() {
        System.out.println("Welcome to Tic Tac Toe Game!");
    }

    public void displayTable() {
        var t = tables.getTable();
        for (char[] row : t) {
            for (char col : row) {
                System.out.printf("%3c", col);
            }
            System.out.println("");
        }
    }

    public void playerInput() {
        System.out.print("Please input Row and Column : ");
        Scanner sc = new Scanner(System.in);
        int row = sc.nextInt();
        int col = sc.nextInt();
        if (tables.isCellMarked(row, col)) {
            System.out.println("Other player is mark this point already! \n Please try again");
            playerInput();
        } else {
            tables.setTable(row, col);
            turnCount++;
        }

    }

    public static int getTurnCount() {
        return turnCount;
    }

    public boolean checkContinue() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Do you want to play again ? (y/n)");
        return sc.next().equalsIgnoreCase("y");
    }
    
    public void resetGame() {
        tables = new Tables();
        player = tables.getCurrentPlayer();
        turnCount = 0;
    }

}
