package com.aphidech.gametictactoe;

public class Player {

    char symbol;
    int winCount, lostCount, drawCount;

    Player(char symbol) {
        this.symbol = symbol;
    }

    public char getSymbol() {
        return symbol;
    }

}
